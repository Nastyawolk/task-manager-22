package ru.t1.volkova.tm.api.model;

import ru.t1.volkova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
